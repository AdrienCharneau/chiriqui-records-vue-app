import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'

import HeaderLogo from './components/HeaderLogo.vue'
import HeaderNav from './components/HeaderNav.vue'
import ContentMusicGrid from './components/ContentMusicGrid.vue'
import ContentMusicDetail from './components/ContentMusicDetail.vue'
import ContentAbout from './components/ContentAbout.vue'
import FooterSocials from './components/FooterSocials.vue'
import FooterCopyright from './components/FooterCopyright.vue'

const app = createApp(App);

app.component('header-logo', HeaderLogo);
app.component('header-nav', HeaderNav);
app.component('content-music-grid', ContentMusicGrid);
app.component('content-music-detail', ContentMusicDetail);
app.component('content-about', ContentAbout);
app.component('footer-socials', FooterSocials);
app.component('footer-copyright', FooterCopyright);

app.config.unwrapInjectedRef = true;

app.mount('#app')
